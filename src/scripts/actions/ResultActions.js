import alt from 'MyAlt';
import { createActions } from 'alt-utils/lib/decorators';

@createActions(alt)
class ResultActions {
	constructor() {
		this.generateActions('getResult');
	}
}

export default ResultActions;
