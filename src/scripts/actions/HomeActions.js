import alt from 'MyAlt';
import { createActions } from 'alt-utils/lib/decorators';

@createActions(alt)
class HomeActions {
	constructor() {
		this.generateActions('search');
		this.generateActions('clearOptions');
	}
}

export default HomeActions;
