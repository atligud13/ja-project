function _createRequest(cb) {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = () => {
		if (xmlHttp.readyState === 4) {
			if (xmlHttp.status < 400) {
				cb(
					null,
					{
						status: xmlHttp.status,
						body: xmlHttp.responseText ? JSON.parse(xmlHttp.responseText) : { }
					}
				);
			} else {
				cb(xmlHttp.status);
			}
		}
	};
	return xmlHttp;
}

class HttpUtil {
	static get(url, cb) {
		const xmlHttp = _createRequest(cb);
		xmlHttp.open('GET', url, true);
		xmlHttp.setRequestHeader('Content-Type', 'application/json');
		xmlHttp.send(null);
	}

	static post(url, payload, cb) {
		const xmlHttp = _createRequest(cb);
		xmlHttp.open('POST', url, true);
		xmlHttp.setRequestHeader('Content-Type', 'application/json');
		xmlHttp.send(JSON.stringify(payload));
	}

	static del(url, cb) {
		const xmlHttp = _createRequest(cb);
		xmlHttp.open('DELETE', url, true);
		xmlHttp.setRequestHeader('Content-Type', 'application/json');
		xmlHttp.send(null);
	}
}

export default HttpUtil;
