import React, { Component } from 'react';
import { Link } from 'react-router';
import ReactDom from 'react-dom';
import connectToStores from 'alt-utils/lib/connectToStores';
import HomeStore from 'stores/HomeStore';
import actions from 'actions/HomeActions';
import logo from '../../../assets/ja-logo.png';
import userLogo from '../../../assets/user-icon.png';
import companyLogo from '../../../assets/company-icon.png';
import '../../../styles/modules/SearchBar.scss';

@connectToStores
class SearchBar extends Component {
	constructor(props, context) {
		super(props);
		this.state = {
			searchValue: '',
			searchOptions: []
		};
		this.onInput = this.onInput.bind(this);
		this.onSearchKeyDown = this.onSearchKeyDown.bind(this);
	}

	static getStores(props) {
		return [HomeStore];
	}

	static getPropsFromStores(props) {
		return HomeStore.getState();
	}

	static contextTypes = {
		router: React.PropTypes.object
	};

	onInput(e) {
		this.setState({ searchValue: e.target.value, typingStarted: true });
		actions.search(e.target.value);
	}

	onSearchKeyDown(e) {
		if(e.keyCode === 13) {
			if(this.props.searchOptions.white.items.length > 0) {
				this.context.router.push('/result/' + this.props.searchOptions.white.items[0].hashid);
			} else if(this.props.searchOptions.yellow.items.length > 0) {
				this.context.router.push('/result/' + this.props.searchOptions.yellow.items[0].hashid);
			}
			this.setState({ searchValue: '' });
		}
	}

	getWhiteSearchOption(option, i) {
		return (
			<li className="search-option" key={i}> 
				<Link to={ '/result/' + option.hashid }>
					<img src={userLogo} className="logo" />
					<span className="user-name"> {option.name} </span>
				</Link> 
			</li>
		);
	}

	getYellowSearchOption(option, i) {
		let logo = companyLogo;
		if(option.logo_url) logo = 'https://media.ja.is/' + option.logo_url;
		return (
			<li className="search-option" key={i}> 
				<Link to={ '/result/' + option.hashid }>
					<img src={logo} className="logo" />
					<span className="user-name"> {option.name} </span>
				</Link> 
			</li>
		);
	}

	render() {
		return (
			<div className="search-bar">
				<div className="search-wrapper">
					<img src={logo} className="logo-small" />
					<div className="input-wrapper">
						<input type="search" 
							onChange={this.onInput} 
							value={this.state.searchValue} 
							onKeyDown={this.onSearchKeyDown}
							ref="searchInput"
							className="search-input"
							placeholder="Not what you were looking for? Try again" />
					</div>
				</div>
				<ul className="search-options-list" hidden={this.props.searchOptions.white.items.length === 0 && this.props.searchOptions.yellow.items.length === 0}>
					<li className="options-list-border"> <strong> Individuals </strong> </li>
					{this.props.searchOptions.white.items.map(this.getWhiteSearchOption)}
					<li className="options-list-border"> <strong> Companies and services </strong> </li>
					{this.props.searchOptions.yellow.items.map(this.getYellowSearchOption)}
					<li className="search-option-msg"> <small> Press enter for top result </small> </li>
				</ul>
			</div>
		);
	}
}

export default SearchBar;
