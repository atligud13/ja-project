import React, { Component } from 'react';
import SearchBar from 'components/SearchBar/SearchBar';
import connectToStores from 'alt-utils/lib/connectToStores';
import '../../../styles/modules/Result.scss';
import actions from 'actions/HomeActions';
import HomeStore from 'stores/HomeStore';

@connectToStores
class Result extends Component {
	constructor(props) {
		super(props);
		this.state = {
			result: { }
		};
	}

	static getStores(props) {
		return [HomeStore];
	}

	static getPropsFromStores(props) {
		return HomeStore.getState();
	}

	componentWillMount() {
		let res = this.props.searchOptions.white.items.find(option => option.hashid === this.props.params.id);
		if(res === undefined) res = this.props.searchOptions.yellow.items.find(option => option.hashid === this.props.params.id) || { };
		this.setState({ result: res });
		actions.clearOptions();
	}

	componentWillReceiveProps(props) {
		if(props.params.id !== undefined && props.params.id !== this.state.result.hashid) {
			let res = this.props.searchOptions.white.items.find(option => option.hashid === this.props.params.id);
			if(res === undefined) res = this.props.searchOptions.yellow.items.find(option => option.hashid === this.props.params.id) || { };
			this.setState({ result: res });
			if(this.state.result.hashid) actions.clearOptions();
		}
	}

	render() {
		let banner = 'http://media.ja.is/sala/vorumerkjasida/banner/jaw-394.jpg';
		if(this.state.result.brand_banner_path !== null) banner = this.state.result.brand_banner_path;
		let logo = '';
		if(this.state.result.logo_url) logo = 'https://media.ja.is/' + this.state.result.logo_url;
		let phoneNumber = '';
		if(this.state.result.phone) phoneNumber = this.state.result.phone.pretty;
		return (
			<div>
				<SearchBar />
				<div className="result-container">
					<div className="banner-row">
						<img src={banner} 
							className="banner" />
					</div>
					<div className="logo-row">
						<img src={logo} hidden={!this.state.result.logo_url}/>
						<span> {this.state.result.slogan} </span>
						<strong className="phone-number"> {phoneNumber} </strong>
					</div>
					<h2 className="name"> {this.state.result.name} </h2>
					<ul className="properties-list">
						<li> {this.state.result.address} </li>
						<li> {this.state.result.postal_station} </li>
						<li> {this.state.result.email} </li>
						<li> {this.state.result.hours} </li>
					</ul>
				</div>
			</div>
		);
	}
}

export default Result;
