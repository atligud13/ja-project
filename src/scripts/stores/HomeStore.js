import alt from 'MyAlt';
import { createStore, bind } from 'alt-utils/lib/decorators';
import HomeActions from 'actions/HomeActions';
import SearchAPI from 'api/SearchAPI';

@createStore(alt)
class HomeStore {
	constructor() {
		this.searchOptions = { white: { items: [] }, yellow: { items: [] } };
		this.searchValue = '';
	}

	@bind(HomeActions.search)
	onSearch(searchValue) {
		if(searchValue) {
			SearchAPI.getSearchResults(searchValue, (err, res) => {
				if(res) {
					this.setState({ searchOptions: res.body });
				} else {
					this.setEmptyOptions();
				}
			});
		} else {
			this.setEmptyOptions();
		}
		this.searchValue = searchValue;
	}

	@bind(HomeActions.clearOptions)
	onClearOptions() {
		const count = this.searchOptions.white.items.length + this.searchOptions.yellow.items.length;
		const dataObject = { searchValue: this.searchValue, count: count };
		SearchAPI.saveSearch(dataObject, (err, res) => {
			if(err) console.log('Unable to save search values!');
			if(res) console.log('Search values successfully saved');
		});
		this.searchOptions = { white: { items: [] }, yellow: { items: [] } };
		this.emitChange();
	}

	setEmptyOptions() {
		this.setState({ searchOptions: { white: { items: [] }, yellow: { items: [] } } });
	}
}

export default HomeStore;
