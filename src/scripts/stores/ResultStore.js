import alt from 'MyAlt';
import { createStore, bind } from 'alt-utils/lib/decorators';
import HomeStore from 'stores/HomeStore';
import ResultActions from 'actions/ResultActions';
import SearchAPI from 'api/SearchAPI';

@createStore(alt)
class ResultStore {
	constructor() {
		this.result = { };
	}

	@bind(ResultActions.getResult)
	onGetResult(id) {
		console.log(this);
	}
}

export default ResultStore;
