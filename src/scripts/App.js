import React, { Component } from 'react';
import { Router, Route, browserHistory, IndexRoute, Navigation } from 'react-router';
import 'font-awesome-webpack';
import Home from 'components/Home/Home';
import Container from 'components/Container';
import Result from 'components/Result/Result';
import '../styles/modules/App.scss';

class App extends Component {
	constructor(props, context) {
		super(props);
	}

	static contextTypes = {
		router: React.PropTypes.object
	};

	render() {
		return (
			<div>
				<Router history={browserHistory}>
					<Route path="/" component={ Container } >
						<IndexRoute component={ Home } />
						<Route path="/result/:id" component={ Result } />
					</Route>
				</Router>
			</div>
		);
	}
}

export default App;
