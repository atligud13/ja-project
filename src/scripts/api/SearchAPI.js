import HttpUtil from 'utils/HttpUtil';

const API_URL = 'http://localhost:4001';
class SearchAPI {
	static getSearchResults(searchValue, cb) {
		HttpUtil.get(API_URL + '/search/?searchValue=' + searchValue, cb);
	}

	static saveSearch(dataObject, cb) {
		HttpUtil.post(API_URL + '/search', dataObject, cb);
	}
}

export default SearchAPI;
