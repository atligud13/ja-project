var express = require('express');
var mongoose = require('mongoose');
var request = require('request');
var bodyParser = require('body-parser');
var utf8 = require('utf8');
var app = express();
var JA_API = 'https://api.ja.is/search';
mongoose.connect('mongodb://localhost/test');

/* Init */
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	/* We're connected! */
	app.listen(4001, function () {
		console.log('API listening at localhost:4001');
	});
});

/* Setting up origin headers */
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* Preparing data model */
var searchSchema = mongoose.Schema({
	value: String,
	count: Number
});

var Search = mongoose.model(
	'Search',
	searchSchema);

/* End points */
app.get('/search', (req, res) => {
	var searchValue = req.query.searchValue;
	var encodedValue = utf8.encode(searchValue);
	var url = JA_API + '?q=' + encodedValue + '&access_code=rzh1v2iokg2rbwdofm650yjjf70jfmyx0rpa6bdf';
	request(url, function(error, response, body) {
		if(!error && response.statusCode === 200) {
			res.send(body);
		} else {
			res.status(response.statusCode).send('Something went wrong!');
		}
	});
});

app.post('/search', (req, res) => {
	var search = new Search({
		value: req.body.searchValue,
		count: req.body.count
	});

	search.save(function(err) {
		if(err) res.status(500).send();
		else res.status(200).send();
	});
});

app.get('/searchvalues', (req, res) => {
	Search.find({}, function(err, data) {
		if(err) res.status(500).send();
		else res.send(data);
	});
});

module.exports = this;