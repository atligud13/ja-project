var express = require('express');
var request = require('request');
var utf8 = require('utf8');
var app = express();
var JA_API = 'https://api.ja.is/search';

app.listen(4001, function () {
	console.log('Proxy listening at localhost:4001');
});

/* Setting up origin headers */
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Content-Type', 'application/json; charset=utf-8');
	next();
});

app.get('/search', (req, res) => {
	var searchValue = req.query.searchValue;
	var encodedValue = utf8.encode(searchValue);
	var url = JA_API + '?q=' + encodedValue + '&access_code=rzh1v2iokg2rbwdofm650yjjf70jfmyx0rpa6bdf';
	request(url, function(error, response, body) {
		if(!error && response.statusCode === 200) {
			res.send(body);
		} else {
			res.status(response.statusCode).send('Something went wrong!');
		}
	});
});

app.post('/search', (req, res) => {
	/* Not connected to any dbs so we just play it cool */
	res.status(200).send();
});

module.exports = this;