/* This file serves as an entry point to the application.
   This way I can fire up the webpack server and the api server together */

var webPackServer = require('./webpackServer');
if(process.argv[2] == 'db') var apiServer = require('./apiServer');
else var proxyServer = require('./proxyServer');